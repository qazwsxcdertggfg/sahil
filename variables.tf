variable "ami" {
  type        = string
  description = "Linux AMI ID in ap-south-1 Region"
  default     = "ami-0cca134ec43cf708f"
}

variable "instance_type" {
  type        = string
  description = "Instance Type"
  default     = "t2.micro"
}

variable "name_tag" {
  type        = string
  description = "Name of the EC2 instance"
  default     = "My EC2 Instance"
}